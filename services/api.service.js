'use strict'
const { get, has } = require('lodash')
const E = require('moleculer-web').Errors
const ApiGateway = require('moleculer-web')

module.exports = {
  name: 'api',
  mixins: [ApiGateway],

  // More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
  settings: {
    port: process.env.PORT || 8000,
    cors: {
      // Configures the Access-Control-Allow-Origin CORS header.
      origin: '*',
      // Configures the Access-Control-Allow-Methods CORS header.
      methods: ['GET', 'OPTIONS', 'POST', 'PUT', 'DELETE'],
      // Configures the Access-Control-Allow-Headers CORS header.
      allowedHeaders: '*',
      // Configures the Access-Control-Expose-Headers CORS header.
      exposedHeaders: [],
      // Configures the Access-Control-Allow-Credentials CORS header.
      credentials: true,
      // Configures the Access-Control-Max-Age CORS header.
      maxAge: 3600
    },
    routes: [

      {
        path: '/api',
        authorization: true,
        aliases: {
          'REST product': 'product',
          'POST product/:id/rating': 'product.rating.update',
          'GET product/count': 'product.count',
          'REST receipt': 'receipt',
          'REST unit': 'unit',
        },
        bodyParsers: {
          json: true
        }
      },

      {
        path: '/auth',
        authorization: false,
        aliases: {
          'POST register': 'auth.register',
          'POST login': 'auth.login',
          'POST verify': 'auth.tokenVerify',
          'GET me': 'auth.me'
        },
        bodyParsers: {
          json: true
        }
      },
    ],

    // Serve assets from "public" folder
    assets: {
      folder: 'public'
    }
  },

  methods: {
    async authorize (ctx, route, req, res) {
      let token = false
      switch(true) {
        case has(req, 'query.access_token'): {
          token = get(req, 'query.access_token')
          
          break
        }
        case (has(req, 'headers.authorization') 
        && req.headers['authorization'].startsWith('Bearer')): {
          token = req.headers['authorization'].slice(7)
          break
        }
        default: return Promise.reject(new E.UnAuthorizedError(E.ERR_NO_TOKEN))
      }
      const user = await ctx.call('auth.tokenVerify', { access_token: token })
      if(!user) return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN))

      ctx.meta.user = user
      return Promise.resolve(ctx)
		},
  }
}
